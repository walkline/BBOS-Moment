package com.walkline.util.ui;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;

import com.walkline.app.MomentAppConfig;
import com.walkline.moment.inf.Story;

public class ListStyleButtonField extends Field
{
    public static final int DRAWPOSITION_TOP = 0;
    public static final int DRAWPOSITION_BOTTOM = 1;
    public static final int DRAWPOSITION_MIDDLE = 2;
    public static final int DRAWPOSITION_SINGLE = 3;

    private static final int CORNER_RADIUS = 0; //16;

    private static final int HPADDING = Display.getWidth() <= 320 ? 4 : 6;

    private static int COLOR_BACKGROUND = 0xFFFFFF;
    private static int COLOR_BORDER = 0xBBBBBB;
    private static int COLOR_BACKGROUND_FOCUS = 0x186DEF;

    private static final boolean USING_LARGE_ICON = Display.getWidth()<640 ? false : true;
    private Bitmap _iconStoryTitle;
    private MyLabelFieldWithNewline _labelStoryTitle;
    private MyLabelFieldWithNewline _labelStoryAbstract;

    private String _url;
    //private String _name;
    //private int _id = 0;

    private int _rightOffset;
    private int _leftOffset;
    private int _labelHeight;
    private int _drawPosition = -1;

	private static Font FONT_STORY_TITLE;
	private static Font FONT_STORY_ABSTRACT;

    public ListStyleButtonField(Story story)
    {
        super(USE_ALL_WIDTH | Field.FOCUSABLE);

        _labelStoryTitle = new MyLabelFieldWithNewline(story.getTitle());
        _labelStoryAbstract = new MyLabelFieldWithNewline(story.getAbstract());

        _url = story.getUrl();

        _iconStoryTitle = USING_LARGE_ICON ? Bitmap.getBitmapResource("listIcon_large.png") : Bitmap.getBitmapResource("listIcon_small.png");

        //_iconStoryTitleSize = _iconStoryTitle.getWidth();

        setFontSize();
    }

    public void setDrawPosition(int drawPosition) {_drawPosition = drawPosition;}

    public void layout(int width, int height)
    {
    	/*
    	if (_iconStoryTitle != null)
    	{
            _leftOffset = _iconStoryTitle.getWidth() + HPADDING * 2;    		
    	} else {
    		_leftOffset = HPADDING * 2;
    	}*/

    	_leftOffset = HPADDING;
        _rightOffset = HPADDING;

        if (_labelStoryTitle != null)
        {
        	_labelStoryTitle.layout(width - _leftOffset - _rightOffset, height);
            _labelHeight = _labelStoryTitle.getHeight();
        }

        if (_labelStoryAbstract != null)
        {
        	_labelStoryAbstract.layout(width - _leftOffset - _rightOffset, height);
        	_labelHeight += _labelStoryAbstract.getHeight();
        }

        setExtent(width, _labelHeight + 3 * HPADDING);
    }

    public String getUrl() {return _url;}

    private void setFontSize()
    {
    	FONT_STORY_TITLE = MomentAppConfig.FONT_STORY_TITLE;
    	FONT_STORY_ABSTRACT = MomentAppConfig.FONT_STORY_ABSTRACT;

    	if (_labelStoryTitle != null) {_labelStoryTitle.setFont(FONT_STORY_TITLE);}
    	if (_labelStoryAbstract != null) {_labelStoryAbstract.setFont(FONT_STORY_ABSTRACT);}
    }

    protected void paint(Graphics g)
    {
        // News Title Text
    	if (_labelStoryTitle != null)
    	{
            try
            {
            	g.setFont(FONT_STORY_TITLE);
            	g.pushRegion(_leftOffset, HPADDING, _labelStoryTitle.getWidth(), _labelStoryTitle.getHeight(), 0, 0);
            	_labelStoryTitle.paint(g);
            } finally {g.popContext();}
    	}

    	//News Abstract Text
    	if (_labelStoryAbstract != null)
    	{
    		try
    		{
    			g.setFont(FONT_STORY_ABSTRACT);
    			g.setColor(g.isDrawingStyleSet(Graphics.DRAWSTYLE_FOCUS) ? Color.WHITE : Color.GRAY);
    			g.pushRegion(_leftOffset, _labelStoryTitle.getHeight() + 2 * HPADDING, _labelStoryAbstract.getWidth(), _labelStoryAbstract.getHeight(), 0, 0);
    			_labelStoryAbstract.paint(g);
    		} finally {g.popContext();}
    	}
    }

    protected void paintBackground(Graphics g)
    {
        if(_drawPosition < 0)
        {
            super.paintBackground(g);
            return;
        }

        int oldColour = g.getColor();
        int background = g.isDrawingStyleSet(Graphics.DRAWSTYLE_FOCUS) ? COLOR_BACKGROUND_FOCUS : COLOR_BACKGROUND;

        try {
            if(_drawPosition == 0)
            {
                // Top
                g.setColor(background);
                g.fillRoundRect(0, 0, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, 0, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
            } else if(_drawPosition == 1)
            {
                // Bottom 
                g.setColor(background);
                g.fillRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
            } else if(_drawPosition == 2)
            {
                // Middle
                g.setColor(background);
                g.fillRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + 2 * CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + 2 * CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
            } else {
                // Single
                g.setColor(background);
                g.fillRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
            }
        } finally {g.setColor(oldColour);}
    }

    protected void drawFocus(Graphics g, boolean on)
    {
        if(_drawPosition >= 0)
        {
            boolean oldDrawStyleFocus = g.isDrawingStyleSet(Graphics.DRAWSTYLE_FOCUS);
            try {
                if(on)
                {
                	g.setColor(Color.WHITE);
                    g.setDrawingStyle(Graphics.DRAWSTYLE_FOCUS, true);
                }
                paintBackground(g);
                paint(g);
            } finally {
                g.setDrawingStyle(Graphics.DRAWSTYLE_FOCUS, oldDrawStyleFocus);
            }
        }
    }

    protected boolean keyChar(char character, int status, int time) 
    {
    	switch (character)
    	{
			case Characters.ENTER:
	            clickButton();
	            return true;
        }

        return super.keyChar(character, status, time);
    }

    protected boolean navigationUnclick(int status, int time) 
    {
    	if ((status & KeypadListener.STATUS_FOUR_WAY) == KeypadListener.STATUS_FOUR_WAY)
    	{
        	clickButton();
        	return true;
    	}

    	return super.navigationClick(status, time);
    }

    protected boolean trackwheelClick(int status, int time)
    {
    	if ((status & KeypadListener.STATUS_TRACKWHEEL) == KeypadListener.STATUS_TRACKWHEEL)
    	{
       		clickButton();
       		return true;
    	}

    	return super.trackwheelClick(status, time);
    }

    protected boolean invokeAction(int action) 
    {
    	switch(action)
    	{
    		case ACTION_INVOKE:
           		clickButton();
           		return true;
    	}

    	return super.invokeAction(action);
    }

    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX(1);
        int y = message.getY(1);

        if (x < 0 || y < 0 || x > getExtent().width || y > getExtent().height) {return false;}

        switch (message.getEvent())
        {
            case TouchEvent.UNCLICK:
           		clickButton();
           		return true;
        }

        return super.touchEvent(message);
    }

    public void clickButton() {fieldChangeNotify(0);}

    public void setDirty(boolean dirty) {}
    public void setMuddy(boolean muddy) {}
}