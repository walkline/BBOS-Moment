package com.walkline.util;

public class Enumerations
{
	public static class RefreshActions
	{
		public static final int DAILYSTORIES = 0;
		public static final int STORYDETAILS = 1;
	}
}