package com.walkline.moment;

import com.walkline.moment.inf.DailyStories;
import com.walkline.moment.inf.StoryDetails;

public class BasicAsyncCallback implements AsyncCallback
{
	public void onException(Exception e, Object state) {}
	public void onComplete(DailyStories value, Object state) {}
	public void onComplete(StoryDetails value, Object state) {}
}