package com.walkline.moment;

public class MomentException extends Exception
{
	public MomentException() {super();}

	public MomentException(String message) {super(message);}
}