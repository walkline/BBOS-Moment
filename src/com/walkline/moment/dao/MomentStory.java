package com.walkline.moment.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.Author;
import com.walkline.moment.inf.Photo;
import com.walkline.moment.inf.Story;

public class MomentStory extends MomentObject implements Story
{
	private int _display_style = 0;
	private String _date = "";
	private String _original_url = "";
	private String _column = "";
	private String _short_url = "";
	private boolean _is_liked = false;
	private Author _author;
	private String _title = "";
	private String _url = "";
	private String _abstract = "";
	private String _content = "";
	private Vector _photos = new Vector();
	private int _like_count = 0;
	private int _comments_count = 0;
	private Vector _thumbs = new Vector();
	private String _created_time = "";
	private String _published_time = "";
	private String _share_pic_url = "";
	private String _type = "";
	private int _id = 0;

	public MomentStory(MomentSDK moment, JSONObject jsonObject) throws MomentException
	{
		super(moment, jsonObject);

		JSONObject story = jsonObject;
		if (story != null)
		{
			_display_style = story.optInt("display_style");
			_date = story.optString("date");
			_original_url = story.optString("original_url");
			_column = story.optString("column");
			_short_url = story.optString("short_url");
			_is_liked = story.optBoolean("is_liked");

			JSONObject author = story.optJSONObject("author");
			if (author != null) {_author = new MomentAuthor(moment, author);}
			
			_title = story.optString("title");
			_url = story.optString("url");
			_abstract = story.optString("abstract");
			_content = story.optString("content");

			JSONArray photos = story.optJSONArray("photos");
			if (photos != null)
			{
				Photo photo;
				JSONObject jsonPhoto;
				for (int i=0; i<photos.length(); i++)
				{
					try {
						jsonPhoto = (JSONObject) photos.get(i);
						if (jsonPhoto != null)
						{
							photo = new MomentPhoto(moment, jsonPhoto);
							if (photo != null) {_photos.addElement(photo);}
						}
					} catch (JSONException e) {}
				}
			}

			_like_count = story.optInt("like_count");
			_comments_count = story.optInt("comments_count");

			JSONArray thumbs = story.optJSONArray("thumbs");
			if (thumbs != null)
			{
				Photo thumb;
				JSONObject jsonThumb;
				for (int i=0; i<thumbs.length(); i++)
				{
					try {
						jsonThumb = thumbs.getJSONObject(i);
						if (jsonThumb != null)
						{
							thumb = new MomentPhoto(moment, jsonThumb);
							if (thumb != null) {_thumbs.addElement(thumb);}
						}
					} catch (JSONException e) {}
				}
			}

			_created_time = story.optString("created_time");
			_published_time = story.optString("published_time");
			_share_pic_url = story.optString("share_pic_url");
			_type = story.optString("type");
			_id = story.optInt("id");
		}
	}

	public int getDisplayStyle() {return _display_style;}

	public String getDate() {return _date;}

	public String getOriginalUrl() {return _original_url;}

	public String getColumn() {return _column;}

	public String getShortUrl() {return _short_url;}

	public boolean isLiked() {return _is_liked;}

	public Author getAuthor() {return _author;}

	public String getTitle() {return _title;}

	public String getUrl() {return _url;}

	public String getAbstract() {return _abstract;}

	public String getContent() {return _content;}

	public Vector getPhotos() {return _photos;}

	public int getLikeCount() {return _like_count;}

	public int getCommentsCount() {return _comments_count;}

	public Vector getThumbs() {return _thumbs;}

	public String getCreatedTime() {return _created_time;}

	public String getPublishedTime() {return _published_time;}

	public String getSharePicUrl() {return _share_pic_url;}

	public String getType() {return _type;}

	public int getId() {return _id;}
}