package com.walkline.moment.dao;

import org.json.me.JSONObject;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.Author;

public class MomentAuthor extends MomentObject implements Author
{
	private String _uid = "";
	private String _url = "";
	private String _avatar = "";
	private String _alt = "";
	private String _large_avatar = "";
	private String _id = "";
	private String _name = "";

	public MomentAuthor(MomentSDK moment, JSONObject jsonObject) throws MomentException
	{
		super(moment, jsonObject);

		JSONObject author = jsonObject;
		if (author != null)
		{
			_uid = author.optString("uid");
			_url = author.optString("url");
			_avatar = author.optString("avatar");
			_alt = author.optString("alt");
			_large_avatar = author.optString("large_avatar");
			_id = author.optString("id");
			_name = author.optString("name");
		}
	}

	public String getUID() {return _uid;}

	public String getUrl() {return _url;}

	public String getAvatar() {return _avatar;}

	public String getAlt() {return _alt;}

	public String getLargeAvatar() {return _large_avatar;}

	public String getId() {return _id;}

	public String getName() {return _name;}
}