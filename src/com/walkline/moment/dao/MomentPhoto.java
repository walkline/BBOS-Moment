package com.walkline.moment.dao;

import org.json.me.JSONObject;
import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.Photo;

public class MomentPhoto extends MomentObject implements Photo
{
	private String _tag_name = "";
	private String _description = "";
	private int _id = 0;
	private String _small_url = "";
	private int _small_width = 0;
	private int _small_height = 0;
	private String _medium_url = "";
	private int _medium_width = 0;
	private int _medium_height = 0;
	private String _large_url = "";
	private int _large_width = 0;
	private int _large_height = 0;

	public MomentPhoto(MomentSDK moment, JSONObject jsonObject) throws MomentException
	{
		super(moment, jsonObject);

		JSONObject photo = jsonObject;
		if (photo != null)
		{
			_tag_name = photo.optString("tag_name");
			_description = photo.optString("description");
			_id = photo.optInt("id");

			JSONObject small = photo.optJSONObject("small");
			if (small != null)
			{
				_small_url = small.optString("url");
				_small_width = small.optInt("width");
				_small_height = small.optInt("height");
			}

			JSONObject medium = photo.optJSONObject("medium");
			if (medium != null)
			{
				_medium_url = small.optString("url");
				_medium_width = small.optInt("width");
				_medium_height = small.optInt("height");
			}

			JSONObject large = photo.optJSONObject("large");
			if (large != null)
			{
				_large_url = small.optString("url");
				_large_width = small.optInt("width");
				_large_height = small.optInt("height");
			}
		}
	}

	public String getTagName() {return _tag_name;}

	public String getDescription() {return _description;}

	public int getId() {return _id;}

	public String getSmallUrl() {return _small_url;}

	public int getSmallWidth() {return _small_width;}

	public int getSmallHeight() {return _small_height;}

	public String getMediumUrl() {return _medium_url;}

	public int getMediumWidth() {return _medium_width;}

	public int getMediumHeight() {return _medium_height;}

	public String getLargeUrl() {return _large_url;}

	public int getLargeWidth() {return _large_width;}

	public int getLargeHeight() {return _large_height;}
}