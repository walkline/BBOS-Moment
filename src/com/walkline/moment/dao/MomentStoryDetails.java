package com.walkline.moment.dao;

import org.json.me.JSONObject;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.StoryDetails;

public class MomentStoryDetails extends MomentObject implements StoryDetails
{
	private String _html_body = "";
	private String _image_source = "";
	private int _section_id = 0;
	private String _section_name = "";
	private String _section_thumbnail = "";
	private int _theme_id = 0;
	private String _theme_name = "";
	private String _theme_image = "";
	private int _editor_id = 0;
	private String _editor_name = "";
	private String _editor_avatar = "";
	private String _title = "";
	private String _image = "";
	private byte[] _image_data;
	private String _share_url = "";
	private String _ga_prefix = "";
	private int _type = 0;
	private int _id = 0;
	//private String[] _js;
	//private String[] _css;
	private boolean _is_forbid_reproduced = false;

	public MomentStoryDetails(MomentSDK zhihu, JSONObject jsonObject) throws MomentException
	{
		super(zhihu, jsonObject);

		JSONObject story = jsonObject;
		if (story != null)
		{
			_html_body = story.optString("body");
			_image_source = story.optString("image_source");
			_section_thumbnail = story.optString("section_thumbnail");
			_title = story.optString("title");
			_image = story.optString("image");
			_section_id = story.optInt("section_id");
			_share_url = story.optString("share_url");
			_section_name = story.optString("section_name");
			_ga_prefix = story.optString("ga_prefix");
			_type = story.optInt("type");
			_id = story.optInt("id");
			_theme_id = story.optInt("theme_id");
			_theme_name = story.optString("theme_name");
			_theme_image = story.optString("theme_image");
			_editor_id = story.optInt("editor_id");
			_editor_name = story.optString("editor_name");
			_editor_avatar = story.optString("editor_avatar");
			//_html_body += "禁止转载";
			_is_forbid_reproduced = _html_body.indexOf("禁止转载") != -1;

			/*
			JSONArray jsonArray = story.optJSONArray("js");
			_js = new String[jsonArray.length()];
			for (int i=0; i<jsonArray.length(); i++)
			{
				try {
					_js[i] = (String) jsonArray.get(i);
				} catch (JSONException e) {}
			}

			jsonArray = story.optJSONArray("css");
			_css = new String[jsonArray.length()];
			for (int i=0; i<jsonArray.length(); i++)
			{
				try {
					_css[i] = (String) jsonArray.get(i);
				} catch (JSONException e) {}
			}
			*/
		}
	}

	public String getHTMLBody() {return _html_body;}

	public String getImageSource() {return _image_source;}

	public String getTitle() {return _title;}

	public String getImage() {return _image;}

	public String getShareUrl() {return _share_url;}

	//public String getThumbnail() {return _thumbnail;}

	public String getGaPrefix() {return _ga_prefix;}

	public int getId() {return _id;}

	//public String[] getJS() {return _js;}

	//public String[] getCSS() {return _css;}

	public int getSectionId() {return _section_id;}

	public String getSectionThumbnail() {return _section_thumbnail;}

	public String getSectionName() {return _section_name;}

	public byte[] getImageData() {return _image_data;}

	public void setImageData(byte[] data) {_image_data = data;}

	public int getType() {return _type;}

	public boolean isForbidReproduced() {return _is_forbid_reproduced;}

	public int getThemeId() {return _theme_id;}

	public String getThemeName() {return _theme_name;}

	public String getThemeImage() {return _theme_image;}

	public int getEditorId() {return _editor_id;}

	public String getEditorName() {return _editor_name;}

	public String getEditorAvatar() {return _editor_avatar;}
}