package com.walkline.moment.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.DailyStories;
import com.walkline.moment.inf.Story;
import com.walkline.util.Function;

public class MomentDailyStories extends MomentObject implements DailyStories
{
	private String _date = "";
	private int _total = 0;
	private Vector _stories = new Vector();

	public MomentDailyStories(MomentSDK moment, JSONObject jsonObject) throws MomentException
	{
		super(moment, jsonObject);

		JSONObject dailyStories = jsonObject;
		if (dailyStories != null)
		{
			_date = dailyStories.optString("date");
			_total = dailyStories.optInt("total");

			JSONArray storiesArray = dailyStories.optJSONArray("posts");
			JSONObject storyObject;
			Story story;
			if (storiesArray != null)
			{
				for (int i=0; i<storiesArray.length(); i++)
				{
					try {
						storyObject = (JSONObject) storiesArray.get(i);

						story = new MomentStory(moment, storyObject);
						if (story != null) {_stories.addElement(story);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public String getDate() {return _date;}

	public Vector getStories() {return _stories;}

	public int getTotal() {return _total;}
}