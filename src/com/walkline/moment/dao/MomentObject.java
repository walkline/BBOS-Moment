package com.walkline.moment.dao;

import org.json.me.JSONObject;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;

public class MomentObject implements com.walkline.moment.inf.Object
{
	protected MomentSDK zhihu;
	protected JSONObject jsonObject;

	public MomentObject(MomentSDK pZhihu, JSONObject pJsonObject) throws MomentException
	{
		if ((pZhihu == null) || (pJsonObject == null)) {
			throw new MomentException("Unable to create Moment Object.");
		}

		zhihu = pZhihu;
		jsonObject = pJsonObject;
	}
}