package com.walkline.moment;

import org.json.me.JSONObject;
import org.json.me.JSONTokener;

import com.walkline.app.MomentAppConfig;
import com.walkline.moment.dao.MomentDailyStories;
import com.walkline.moment.dao.MomentStoryDetails;
import com.walkline.moment.inf.DailyStories;
import com.walkline.moment.inf.StoryDetails;
import com.walkline.util.Function;
import com.walkline.util.StringUtility;
import com.walkline.util.network.HttpClient;
import com.walkline.util.network.MyConnectionFactory;

public class MomentSDK
{
	protected HttpClient _http;

	public static MomentSDK getInstance() {return new MomentSDK();}

	protected MomentSDK()
	{
		MyConnectionFactory cf = new MyConnectionFactory();

		_http = new HttpClient(cf);
	}

	private DailyStories getDailyStories(JSONObject jsonObject) throws MomentException {return new MomentDailyStories(this, jsonObject);}
	public DailyStories getDailyStories(String date) throws MomentException {return getDailyStories(date, null, null);}
	public DailyStories getDailyStories(final String date, final AsyncCallback listener, final Object state)
	{
		if (listener != null) {
			new Thread() {
				public void run() {
					try {
						DailyStories result = null;
						result = getDailyStories(date);
						listener.onComplete(result, null);
					} catch (Exception e) {
						listener.onException(e, null);
					}
				}
			}.start();

			return null;
		} else {
			DailyStories result = null;
			JSONObject jsonObject = new JSONObject();

			try {
				String api = "";

				if (date != null)
				{
					api = StringUtility.replace(MomentAppConfig.queryBeforeStoriesRequestURL, "#DATE#", date);	
				} else {
					api = MomentAppConfig.queryCurrentStoriesRequestURL;
				}

				jsonObject = doRequest(api);

				result = (jsonObject != null ? getDailyStories(jsonObject) : null);
			} catch (MomentException e) {Function.errorDialog(e.toString());}

			return result;
		}
	}

	private StoryDetails getStoryDetails(JSONObject jsonObject) throws MomentException {return new MomentStoryDetails(this, jsonObject);}
	public StoryDetails getStoryDetails(String url) throws MomentException {return getStoryDetails(url, null, null);}
	public StoryDetails getStoryDetails(final String url, final AsyncCallback listener, final Object state)
	{
		if (listener != null) {
			new Thread() {
				public void run() {
					try {
						StoryDetails result = null;
						result = getStoryDetails(url);
						listener.onComplete(result, null);
					} catch (Exception e) {
						listener.onException(e, null);
					}
				}
			}.start();

			return null;
		} else {
			StoryDetails result = null;
			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject = doRequest(url);

				result = (jsonObject != null ? getStoryDetails(jsonObject) : null);
			} catch (MomentException e) {Function.errorDialog(e.toString());}

			return result;
		}
	}




//**************************************************************************************************
//
//             //              //      //    //      ////////////////            //      //        
//             //            //    //  //  //                    //          //  //  //  //        
// //////////  //          //////////  ////        //          //      //        //      //        
//     //    ////////////              //      //  //  //    //    //  //  ////////////  ////////  
//     //      //      //    ////////    ////////  //    //  //  //    //      ////    //    //    
//     //      //      //    //    //              //        //        //    //  ////    //  //    
//     //      //      //    ////////  //          //    //  //  //    //  //    //  //  //  //    
//     //      //      //    //    //  //    //    //  //    //    //  //      //        //  //    
//     ////////        //    ////////  //  //      //        //        //  //////////    //  //    
// ////      //        //    //    //  ////    //  //      ////        //    //    //      //      
//         //          //    //    //  //      //  //                  //      ////      //  //    
//       //        ////      //  ////    ////////  //////////////////////  ////    //  //      //  
//
//**************************************************************************************************

	public byte[] doRequestRAW(String imageUrl) throws MomentException
	{
		byte[] result = null;
		StringBuffer responseBuffer = null;

		try {
			responseBuffer = _http.doGet(imageUrl);				

			if ((responseBuffer == null) || (responseBuffer.length() <= 0))
			{
				result = null;
			} else {
				result = responseBuffer.toString().getBytes();				
			}
		} catch (Exception e) {
			throw new MomentException("[doRequestRAW Exception]\n\n" + e.getMessage());
		} catch (Throwable t) {
			throw new MomentException("[doRequestRAW Throwable]\n\n" + t.getMessage());
		}

		return result;
	}

	private JSONObject doRequest(String api) throws MomentException
	{
		StringBuffer responseBuffer = new StringBuffer();
		JSONObject result = new JSONObject();

		try {
			responseBuffer = _http.doGet(api);

			if ((responseBuffer == null) || (responseBuffer.length() <= 0))
			{
				result = null;
			} else {
				result = new JSONObject(new JSONTokener(new String(responseBuffer.toString().getBytes(), "utf-8")));
				//result = new JSONObject(new JSONTokener(new String(responseBuffer.toString().getBytes(), "utf-8")));
			}
		} catch (Exception e) {
			throw new MomentException(e.getMessage());
		} catch (Throwable t) {
			throw new MomentException(t.getMessage());
		}

		return result;
	}
}