package com.walkline.moment.inf;

public interface Photo
{
	public String getTagName();

	public String getDescription();

	public int getId();

	public String getSmallUrl();

	public int getSmallWidth();

	public int getSmallHeight();

	public String getMediumUrl();

	public int getMediumWidth();

	public int getMediumHeight();

	public String getLargeUrl();

	public int getLargeWidth();

	public int getLargeHeight();
}