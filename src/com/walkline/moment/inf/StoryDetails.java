package com.walkline.moment.inf;

public interface StoryDetails
{
	public String getHTMLBody();

	public String getImageSource();	//图片来源

	public String getTitle();

	public String getImage();		//封面大图

	public byte[] getImageData();

	public void setImageData(byte[] data);

	public int getSectionId();

	public String getSectionThumbnail();

	public String getSectionName();

	public int getThemeId();

	public String getThemeName();

	public String getThemeImage();

	public int getEditorId();

	public String getEditorName();

	public String getEditorAvatar();

	public String getShareUrl();	//原文地址

	//public String getThumbnail();	//封面预览图

	public String getGaPrefix();	//Unknown

	public int getType();			//Unknown

	public int getId();			//新闻ID

	//public String[] getJS();

	//public String[] getCSS();

	public boolean isForbidReproduced();
}