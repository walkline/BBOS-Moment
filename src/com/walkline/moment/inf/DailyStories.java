package com.walkline.moment.inf;

import java.util.Vector;

public interface DailyStories
{
	public String getDate();

	public int getTotal();

	public Vector getStories();
}