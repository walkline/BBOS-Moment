package com.walkline.moment.inf;

import java.util.Vector;


public interface Story
{
	public int getDisplayStyle();

	public String getDate();

	public String getOriginalUrl();

	public String getColumn();

	public String getShortUrl();

	public boolean isLiked();

	public Author getAuthor();

	public String getTitle();

	public String getUrl();

	public String getAbstract();

	public String getContent();

	public Vector getPhotos();

	public int getLikeCount();

	public int getCommentsCount();

	public Vector getThumbs();

	public String getCreatedTime();

	public String getPublishedTime();

	public String getSharePicUrl();

	public String getType();

	public int getId();
}