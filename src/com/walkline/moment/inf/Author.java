package com.walkline.moment.inf;

public interface Author
{
	public String getUID();

	public String getUrl();

	public String getAvatar();

	public String getAlt(); //新浪微博地址

	public String getLargeAvatar();

	public String getId();

	public String getName();
}