package com.walkline.app;

import net.rim.device.api.ui.UiApplication;

import com.walkline.screen.MomentScreen;

public class MomentApp extends UiApplication
{
    public static void main( String[] args )
    {
        MomentApp theApp = new MomentApp();
        theApp.enterEventDispatcher();
    }

    public MomentApp() {pushScreen(new MomentScreen());}
}