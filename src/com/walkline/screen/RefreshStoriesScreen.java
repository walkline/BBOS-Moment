package com.walkline.screen;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.walkline.moment.MomentException;
import com.walkline.moment.MomentSDK;
import com.walkline.moment.inf.DailyStories;
import com.walkline.moment.inf.StoryDetails;
import com.walkline.util.Enumerations.RefreshActions;
import com.walkline.util.Function;
import com.walkline.util.network.MyConnectionFactory;

public class RefreshStoriesScreen extends PopupScreen
{
	Thread thread=null;
	MomentSDK _moment;
	String _date;
	DailyStories _dailyStories;
	StoryDetails _storyDetails;

	public RefreshStoriesScreen(MomentSDK moment, String date, int action)
	{
		super(new VerticalFieldManager());

		_moment = moment;
		_date = date;

		add(new LabelField("Please wait....", Field.FIELD_HCENTER));

		switch (action)
		{
			case RefreshActions.DAILYSTORIES:
				thread = new Thread(new DailyStoriesRunnable());
				break;
			case RefreshActions.STORYDETAILS:
				thread = new Thread(new StoryDetailsRunnable());
				break;
		}

		thread.start();
	}

	class DailyStoriesRunnable implements Runnable
	{
		public void run()
		{
			try
			{
				_dailyStories = _moment.getDailyStories(_date);

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MomentException e) {Function.errorDialog(e.toString());}
		}
	}

	class StoryDetailsRunnable implements Runnable
	{
		public void run()
		{
			try
			{
				_storyDetails = _moment.getStoryDetails(_date);
				if (_storyDetails != null)
				{
					if (!_storyDetails.getImage().equalsIgnoreCase(""))
					{
						MyConnectionFactory cf = new MyConnectionFactory();
		                ConnectionDescriptor descriptor = cf.getConnection(_storyDetails.getImage());

		                if (descriptor == null) {return;}

		                HttpConnection httpConnection = (HttpConnection) descriptor.getConnection();
		                InputStream inputStream = null;

						try {
							inputStream = httpConnection.openInputStream();

							if (inputStream.available() > 0)
							{
								byte[] data = IOUtilities.streamToBytes(inputStream);

								if (data.length == httpConnection.getLength()) {_storyDetails.setImageData(data);}
							}

							inputStream.close();
							httpConnection.close();
						} catch (IOException e) {}
					}
				}

				Application.getApplication().invokeLater(new Runnable() {public void run() {onClose();}});
			} catch (MomentException e) {Function.errorDialog(e.toString());}
		}
	}

	public DailyStories getDailyStories() {return _dailyStories;}

	public StoryDetails getStoryDetails() {return _storyDetails;}

	public boolean onClose()
	{
		if (thread != null)
		{
			try {
				thread.interrupt();
				thread = null;
			} catch (Exception e) {}
		}

		try {
			UiApplication.getUiApplication().popScreen(this);	
		} catch (Exception e) {}

		return true;
	}

	protected boolean keyChar(char key, int status, int time)
	{
		if (key == Characters.ESCAPE)
		{
			onClose();

			return true;
		}

		return super.keyChar(key, status, time);
	}
} 