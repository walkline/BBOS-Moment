package com.walkline.screen;

import java.io.IOException;

import javax.microedition.content.ContentHandler;
import javax.microedition.content.ContentHandlerException;
import javax.microedition.content.Invocation;
import javax.microedition.content.Registry;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.walkline.app.MomentAppConfig;
import com.walkline.util.ui.VerticalButtonFieldSet;

public class AboutScreen extends PopupScreen
{
	public AboutScreen()
	{
		super(new VerticalFieldManager(FOCUSABLE | NO_VERTICAL_SCROLL));

		setBorder(MomentAppConfig.border_popup_Transparent);
		setBackground(MomentAppConfig.bg_popup_Transparent);

    	LabelField labelAbout = new LabelField("关于", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	labelAbout.setFont(MomentAppConfig.FONT_ABOUT_TITLE);
    	labelAbout.setPadding(0, 0, 1, 0);

    	LabelField labelTitle = new LabelField(MomentAppConfig.APP_TITLE, USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	labelTitle.setFont(MomentAppConfig.FONT_ABOUT_LARGE);

    	LabelField labelVersion = new LabelField("版本：" + ApplicationDescriptor.currentApplicationDescriptor().getVersion(), USE_ALL_WIDTH | LabelField.ELLIPSIS | LabelField.RIGHT);
    	labelVersion.setFont(MomentAppConfig.FONT_ABOUT_SMALL);

		VerticalFieldManager vfmTitle = new VerticalFieldManager(FIELD_VCENTER);
    	vfmTitle.add(labelAbout);
    	vfmTitle.add(new SeparatorField());
    	vfmTitle.add(labelTitle);
    	vfmTitle.add(labelVersion);

		VerticalFieldManager vfmContent = new VerticalFieldManager(VERTICAL_SCROLL);
		HorizontalFieldManager horizontalContactTitle = new HorizontalFieldManager();
		HorizontalFieldManager horizontalShortcutTitle = new HorizontalFieldManager();
		VerticalButtonFieldSet vbf = new VerticalButtonFieldSet(USE_ALL_WIDTH);

    	LabelField labelIntro = new LabelField("知乎日报客户端，适用于BBOS6及以上系统。");

    	LabelField labelContact = new LabelField("联系方式：", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	labelContact.setFont(MomentAppConfig.FONT_ABOUT_HEADLINE);

    	LabelField labelAuthor = addLabel("\u2022 Walkline Wang");
    	LabelField labelEmail = addLabel("\u2022 电子邮件：walkline@gmail.com");
    	LabelField labelWeibo = addLabel("\u2022 新浪微博：@Walkline");

    	LabelField labelShortcut = new LabelField("快捷键：", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	labelShortcut.setFont(MomentAppConfig.FONT_ABOUT_HEADLINE);

    	LabelField shortB=addLabel("\u2022 B：列表滚动到底部");
    	LabelField shortT=addLabel("\u2022 T：列表滚动到顶部");
    	LabelField shortL=addLabel("\u2022 L：获取最新新闻");
    	LabelField shortH=addLabel("\u2022 H：获取热门新闻");
    	LabelField shortS=addLabel("\u2022 S：获取专题分类");
    	LabelField shortC=addLabel("\u2022 C：选择往期新闻");
    	LabelField shortN=addLabel("\u2022 N：下一期新闻");
    	LabelField shortP=addLabel("\u2022 P：上一期新闻");
    	LabelField shortA=addLabel("\u2022 A：显示本窗口");
    	LabelField shortX=addLabel("\u2022 X：退出程序");

    	ButtonField btnWriteAReview = new ButtonField("编写评论", ButtonField.NEVER_DIRTY | ButtonField.CONSUME_CLICK);
    	btnWriteAReview.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				try
	            {
	                openAppWorld(MomentAppConfig.BBW_APPID);
	            } catch(final Exception e)
	            {
	                UiApplication.getUiApplication().invokeLater(new Runnable()
	                {
	                    public void run()
	                    {
	                    	if(e instanceof ContentHandlerException)
	                    	{
	                    		Dialog.alert("BlackBerry World is not installed!");
	                    	} else {
	                    		Dialog.alert("Problems opening App World: " + e.getMessage());
	                    	}
	                    }
	                });
	            }
			}
		});

    	ButtonField btnBrowseOtherApps = new ButtonField("浏览其它软件", ButtonField.NEVER_DIRTY | ButtonField.CONSUME_CLICK);
    	btnBrowseOtherApps.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				BrowserSession browser=Browser.getDefaultSession();
	    		browser.displayPage("http://appworld.blackberry.com/webstore/vendor/69061");
			}
		});

    	horizontalContactTitle.add(labelContact);
    	horizontalContactTitle.add(new LabelField("", LabelField.FOCUSABLE));
    	vfmContent.add(labelIntro);
    	vfmContent.add(new LabelField());
    	vfmContent.add(horizontalContactTitle);
    	vfmContent.add(labelAuthor);
    	vfmContent.add(labelEmail);
    	vfmContent.add(labelWeibo);
    	vfmContent.add(new LabelField());
    	
    	horizontalShortcutTitle.add(labelShortcut);
    	horizontalShortcutTitle.add(new LabelField("", LabelField.FOCUSABLE));
    	vfmContent.add(horizontalShortcutTitle);
    	vfmContent.add(shortB);
    	vfmContent.add(shortT);
    	vfmContent.add(shortL);
    	vfmContent.add(shortH);
    	vfmContent.add(shortS);
    	vfmContent.add(shortC);
    	vfmContent.add(shortN);
    	vfmContent.add(shortP);
    	vfmContent.add(shortA);
    	vfmContent.add(shortX);
    	vfmContent.add(new LabelField());
    	vbf.add(btnWriteAReview);
    	vbf.add(btnBrowseOtherApps);
    	vfmContent.add(vbf);

    	add(vfmTitle);
    	add(vfmContent);
	}

	protected void openAppWorld(String myContentId) throws IllegalArgumentException, ContentHandlerException, SecurityException, IOException 
    {
        Registry registry = Registry.getRegistry(MomentScreen.class.getName());
        Invocation invocation = new Invocation( null, null, "net.rim.bb.appworld.Content", true, ContentHandler.ACTION_OPEN );

        invocation.setArgs(new String[] {myContentId});

        boolean mustExit = registry.invoke(invocation);
        if(mustExit) {UiApplication.getUiApplication().popScreen(this);}
    }

	private LabelField addLabel(String label)
	{
		return new LabelField(label, USE_ALL_WIDTH | LabelField.ELLIPSIS);
	}

	protected void paintBackground(Graphics g) {}

	protected boolean keyChar(char character, int status, int time)
	{
		switch (character)
		{
			case Characters.ESCAPE:
				UiApplication.getUiApplication().popScreen(this);
				return true;
		}

		return super.keyChar(character, status, time);
	}
}